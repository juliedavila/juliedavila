---
layout: markdown_page
title: "Julie Davila's README"
---

## Julie Davila's README

**VP, Product Security**

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before. 

It’s also a well-intentioned effort at building some trust by being intentionally vulnerable, and to share my ideas of a good working relationship to reduce the anxiety of people who might be on my team.

Please feel free to contribute to this page by opening a merge request. 

## Related pages

[LinkedIn](https://linkedin.com/in/thisjulie)

[Website](https://juliedavila.com)

## About me

**Origin**
👋🏼 Born and raised on the West Coast to Mexican immigrants, I found myself in the Washington D.C area after joining the US Army shortly after high school. 

**Interesting bits**
I'm a life-long athelete having played ice hockey for most of my life; if I'm not working or doing family stuff, I'm probably doing something hockey related. I'm also an avid snowboarder and endurance athlete (I once competed in a 55-mile weighted hike over a period of 20 hours). 

I also consider myself a voracious reader and enjoy text across a variety of categories. Depending on the day you might catch me reading something philosophical (think Montaigne, stoicism, etc), something professional (like Principles by Ray Dalio), or something else entirely (like Braiding Sweetgrass or The Choice).

## How you can help me

- **Tell me to RTFM.** I generally try to learn information in a "just in time" fashion given the bottomless volume of content that exists. There might be times where I ask or propose something with limited context (initially), and to the extent that there is a doc/issue/MR/slack thread/etc that I can use to get up to speed it would be really helpful if you provide that to me. It's totally fine for that to be the bulk of your response. 
- Bring up concerns (of any kind) to me earlier vs later. Direct and precise language is important to me.

## My working style

- I am biased toward direct and specific communication with an appreciation for nuance. This is how I like to communicate with others.
- I prefer writing over meetings
- When I come up with ideas I prefer to come up with something that is likely to suffice as a `Version 0.1` with the expectation that it's almost never going to be the perfect call at the start. I prefer for ideas to get better over time versus attempting for Day-1 perfection.
- Email is not the place to reach me for immediate response. Assume it might be up to 24 hours before I reply to an email.

## What I assume about others

- **Positive intent:** It's possible you might disagree with me on something and that you vocalize (or write out) this disagreement. I assume it's not personal and that you want to work together toward finding the optimal path forward.
- **You are good at your job:** if you were hired for a role, I assume you're really solid at operating within that capacity and that you're likely to know more about that than me (even within my own teams).
- **You'll be frank with me:** you say what mean and mean what you say. If you love something, you'll tell me. If you really don't like something, you'll tell me.
- **You are more than your job:** you are an interesting person independent of your job (friends, family, hobbies, interests, etc) and there are things in your life which are more important than what you do as a career.

## What I want to earn

- **Trust:** I want conduct myself in such a way that makes you feel like I see you, understand your needs, and your constraints. Trust takes time to earn and a single moment to destroy; it is sacred.
- **Respect:** through following through with what I say I will do, living up to GitLab's values, leading with kindness and treating everyone with the dignity they deserve.

## Communicating with me

- **Simple and direct communication.** Is a project not going well? Is your plate way too full? Did I make a bad choice? Do you need more support? Please, tell me.
- **Have opinions, and challenge mine.** When there is a problem and we're discussing it, I really value hearing your thoughts on what a potential path could be. Equally, you should challenge my ideas whenever you feel something doesn't quite add up. We are better together.
- **How to contact me**
    - Add me to MRs or on issues for communicating on topics/content that warrant persistent communication or documentation
    - Slack me for real-time. 
        - **Regarding DMs**: I prefer them when the topic is confidential or sensitive in nature or otherwise exclusively personal. Even if we're discussing a project or task you're working on, I prefer to do it in a public channel so that others are afforded additional context and the ability to contribute.  
    - For true emergencies: call or text me via Signal.
- **Routine**
    - I'm a morning person and usually wake up at 6 AM (US Eastern) and tend to get started with work around 8 AM, or sometimes 9 AM depending on family obligations. I usually finsish my dedicated work time around 6 PM and sometimes do a bit of note taking/reading in the evening over some music.
    - During the workday, I generally try to keep all of my deep work tasks in the morning and any softer work (1:1s/email responses, etc) to the latter half of the day

## Meetings 

I prefer to avoid sync meetings on Mondays and Fridays, these are my sacred days where I can do extended focused work. If you absolutely need to have a meeting on one of these days, please reach out to me directly on Slack. Otherwise, I'll generally push back via "Propose new time" with a time inside of Tue/Wed/Thu .

